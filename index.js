let num = 2;

let getCube = num*num*num;

console.log(`The cube of ${num} is ${getCube}`);

let address = [
	houseNum = "258 Washington Ave NW",
	state = "California",
	zipCode = 9001
];

console.log(`I live at ${address[0]}, ${address[1]} ${address[2]}`);

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"

};

/*console.log(animal);
*/

const { name, type, weight, measurement } = animal; 
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}. `);

let numbers = [1,2,3,4,5];

numbers.forEach((element) => {console.log(element)});

let reduceNumber = 0;

const sumWithReduce = numbers.reduce((accumulator, currentValue) => accumulator + currentValue, reduceNumber);

console.log(sumWithReduce);


class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);
